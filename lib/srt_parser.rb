class SrtParser
  class WrongFilePath < StandardError; end
  def initialize(file_path)
    @content = File.read(file_path).split("\n\n")
  rescue Errno::ENOENT
    raise WrongFilePath
  end

  def parse
    @content.map do |line|
      LineParser.new(line).parse
    end
  end

  class LineParser
    def initialize(line)
      @line = line
    end

    def parse
      _, time, *content = @line.split("\n")
      content = content.join("\n")
      start_time, end_time = parse_time(time)
      Line.new(start_time, end_time, content)
    end

    private

    def parse_time(time)
      matched = time.match /^[ ]*(\d{2}:\d{2}:\d{2},\d{1,3})[ ]+-->[ ]+(\d{2}:\d{2}:\d{2},\d{1,3})[ ]*$/
      [matched[1], matched[2]].map { |time| to_milliseconds(time) }
    end

    def to_milliseconds(time)
      captures = time.match(/(\d{2}):(\d{2}):(\d{2}),(\d{1,3})/).captures
      captures[-1] = captures.last.ljust(3, '0')
      hours, minutes, seconds, milliseconds = captures.map(&:to_i)
      (hours * 1.hour + minutes * 1.minute + seconds * 1.second) * 1000 + milliseconds
    end
  end

  class Line
    include Normalizer

    attr_reader :start_time, :end_time, :content, :normalized_content

    def initialize(start_time, end_time, content)
      @start_time = start_time
      @end_time = end_time
      @content = content
      normalize_content
    end

    def to_h
      {
        start_time: @start_time,
        end_time: @end_time,
        content: @content,
        normalized_content: @normalized_content
      }
    end

    private

    def normalize_content
      @normalized_content = normalize(@content)
    end
  end
end

class SlackInspect
  def initialize(app)
    @app = app
  end

  def call(env)
    env['PATH_INFO'] = '/api/slack/inspect' if Rack::Utils.parse_query(env["QUERY_STRING"])['text'] =~ /^\s*info/
    @app.call(env)
  end
end
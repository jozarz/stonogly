module Normalizer
  def normalize(string)
    ActiveSupport::Inflector
      .transliterate(string)
      .gsub(/\s/, ' ')
      .gsub(/[.,?!]/, ' ')
      .downcase
      .squeeze(' ')
      .strip
  end
end

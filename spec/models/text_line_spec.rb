require 'rails_helper'

describe TextLine, type: :model do
  let!(:line) { build :text_line, start_time: 6_339_301, end_time: 6_341_451 }

  describe '.by_query' do
    let!(:video) { create :video }
    let!(:line) { create :text_line, normalized_content: 'this is a content', video: video }
    let!(:line2) { create :text_line, normalized_content: 'content is', video: video }
    let!(:line3) { create :text_line, normalized_content: 'not this one', video: video }

    it 'should find all lines with matched normalized content' do
      expect(TextLine.by_query('content')).to match_array [line, line2]
    end

    describe 'ordering' do
      it 'should order by rank' do
        expect(TextLine.by_query('content is')).to eq [line2, line]
      end
    end

    describe 'various type of content' do
      subject { TextLine.by_query(query) }
      ['ThIs is', 'THIS   \n\n  iS', 'This,,, is'].each do |query_|
        let!(:query) { query_ }

        it { is_expected.to eq [line] }
      end
    end


    describe 'contents with order changed' do
      subject { TextLine.by_query(query) }
      ['is this', 'content is this a'].each do |query_|
        let!(:query) { query_ }

        it { is_expected.to eq [line] }
      end
    end
  end

  describe '#start_time_srt' do
    subject { line.start_time_srt }

    it { is_expected.to eq '01:45:39,301' }

    context 'with delimiter passed' do
      subject { line.start_time_srt('.') }

      it { is_expected.to eq '01:45:39.301' }
    end
  end

  describe '#end_time_srt' do
    subject { line.end_time_srt }

    it { is_expected.to eq '01:45:41,451' }

    context 'with delimiter passed' do
      subject { line.end_time_srt('.') }

      it { is_expected.to eq '01:45:41.451' }
    end
  end

  describe '#duration_srt' do
    subject { line.duration_srt }

    it { is_expected.to eq '00:00:02,150' }

    context 'with delimiter passed' do
      subject { line.duration_srt('.') }

      it { is_expected.to eq '00:00:02.150' }
    end
  end
end

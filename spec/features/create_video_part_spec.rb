require 'rails_helper'

describe 'creating video part' do
  around { |example| clean_files(example) }

  let!(:video) { create :video, file: File.new(video_path) }
  let!(:video_path) { 'spec/support/files/video-test-file.mp4' }
  let!(:query) { 'content' }

  subject(:interaction) { Videos::CreatePart.run(query: query) }

  context 'there is no line matching given query' do
    it { is_expected.not_to be_valid }
  end

  context 'there is line matching given query' do
    let!(:text_line) do
      create :text_line,
             video: video,
             normalized_content: 'content',
             start_time: 500,
             end_time: 900,
             content: 'content'
    end

    it { is_expected.to be_valid }

    it 'should remove temporary files' do
      expect { interaction }.not_to change { Dir['tmp/*.gif'].size }
    end

    context 'when line do not have video part' do
      specify { expect { interaction }.to change { Dir['public/gifs/*.gif'].size }.by(1) }

      describe 'created video part' do
        subject(:video_part) { interaction.result.video_part_gif }
        it 'should have correct length' do
          expect(video_part).to have_gif_length(400)
        end
      end
    end

    context 'when line has video part' do
      before { interaction }

      it 'should not create new gif' do
        expect { interaction }.not_to change { Dir['public/gifs/*.gif'].size }
      end
    end
  end
end

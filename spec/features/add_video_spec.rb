require 'rails_helper'

describe 'adding files with video and text lines to database' do
  around { |example| clean_files(example) }

  let!(:video_path) { 'spec/support/files/video-test-file.mp4' }
  let!(:subtitles_path) { 'spec/support/files/srt-test-file.srt' }

  subject(:interaction) do
    Videos::Create.run(
      subtitles_path: subtitles_path,
      video_path: video_path,
      name: 'stonoga'
    )
  end

  context 'when creating video fails' do
    let!(:video_path) { 'obisly-wrong.mp4' }

    it { is_expected.not_to be_valid }

    it 'should not create any object in database' do
      expect { interaction }.not_to change { Video.count }
      expect { interaction }.not_to change { TextLine.count }
    end
  end

  context 'when adding text lines fails' do
    let!(:subtitles_path) { 'obisly-wrong.srt' }

    it { is_expected.not_to be_valid }

    it 'should not create any object in database' do
      expect { interaction }.not_to change { Video.count }
      expect { interaction }.not_to change { TextLine.count }
    end
  end

  context 'happy path' do
    it 'create video in database' do
      expect { interaction }.to change { Video.count }.by(1)
    end

    it 'creates 3 lines in database' do
      expect { interaction }.to change { TextLine.count }.by(3)
    end

    describe 'created line' do
      before { interaction }
      describe 'lines start times' do
        subject { TextLine.pluck(:start_time) }

        it { is_expected.to eq [780, 4860, 7580] }
      end

      describe 'lines end times' do
        subject { TextLine.pluck(:end_time) }

        it { is_expected.to eq [4860, 6620, 10_720] }
      end

      describe 'lines contents' do
        subject { TextLine.pluck(:content) }

        it do
          is_expected.to eq [
            "Dobry wieczór. Coś się... coś się popsuło\ni nie było mnie słychać",
            'To powtórzę jeszcze raz',
            'Wynik wyborczy KWW Stonogi to jest jakaś porażka'
          ]
        end
      end

      describe 'lines normalized contents' do
        subject { TextLine.pluck(:normalized_content) }

        it do
          is_expected.to eq [
            'dobry wieczor cos sie cos sie popsulo i nie bylo mnie slychac',
            'to powtorze jeszcze raz',
            'wynik wyborczy kww stonogi to jest jakas porazka'
          ]
        end
      end
    end
  end
end

require 'spec_helper'

describe SrtParser do
  describe '#parse' do
    subject(:lines) { SrtParser.new('spec/support/files/srt-test-file.srt').parse }

    it 'should return array of SrtParser::Lines', :aggregate_failures do
      expect(subject).to be_a Array
      expect(subject.length).to eq 3
      expect(subject).to be_all { |element| element.is_a? SrtParser::Line }
    end

    describe 'lines start times' do
      subject { lines.map(&:start_time) }

      it { is_expected.to eq [780, 4860, 7580] }
    end

    describe 'lines end times' do
      subject { lines.map(&:end_time) }

      it { is_expected.to eq [4860, 6620, 10_720] }
    end

    describe 'lines contents' do
      subject { lines.map(&:content) }

      it do
        is_expected.to eq [
          "Dobry wieczór. Coś się... coś się popsuło\ni nie było mnie słychać",
          'To powtórzę jeszcze raz',
          'Wynik wyborczy KWW Stonogi to jest jakaś porażka'
        ]
      end
    end

    describe 'lines normalized contents' do
      subject { lines.map(&:normalized_content) }

      it do
        is_expected.to eq [
          'dobry wieczor cos sie cos sie popsulo i nie bylo mnie slychac',
          'to powtorze jeszcze raz',
          'wynik wyborczy kww stonogi to jest jakas porazka'
        ]
      end
    end
  end

  describe SrtParser::LineParser do
    let(:start_time) { '01:45:39,301' }
    let(:end_time) { '01:45:41,451' }
    let(:content) { "content\ncontent\ncontent" }
    let(:line) { "1\n#{start_time} --> #{end_time}\n#{content}" }

    subject { SrtParser::LineParser.new(line).parse }
    it 'should return SrtParser::Line object' do
      expect(subject).to be_a SrtParser::Line
    end

    it 'should get start time in milliseconds' do
      expect(subject.start_time).to eq 6_339_301
    end

    it 'should get end time in milliseconds' do
      expect(subject.end_time).to eq 6_341_451
    end

    it 'should behave good when there is content with new line' do
      expect(subject.content).to eq content
    end

    describe 'various time line formatting' do
      shared_examples 'correct start and end time' do
        it 'should get start time in milliseconds' do
          expect(subject.start_time).to eq 6_339_301
        end

        it 'should get end time in milliseconds' do
          expect(subject.end_time).to eq 6_341_451
        end
      end

      context do
        let(:line) { "1\n#{start_time}    -->   #{end_time}\n#{content}" }
        it_behaves_like 'correct start and end time'
      end

      context do
        let(:line) { "1\n     #{start_time}     -->      #{end_time}     \n#{content}" }
        it_behaves_like 'correct start and end time'
      end

      context 'milliseconds not completed' do
        let(:line) { "1\n 00:00:00,01 --> 00:00:00,1\ncontent" }

        it 'should get start time in milliseconds' do
          expect(subject.start_time).to eq 10
        end

        it 'should get end time in milliseconds' do
          expect(subject.end_time).to eq 100
        end
      end
    end
  end

  describe SrtParser::Line do
    subject { SrtParser::Line.new(start_time, end_time, @content) }
    let(:start_time) { double(:start_time) }
    let(:end_time) { double(:end_time) }

    it 'should leave content alone 'do
      @content = '  lol ążźć lol '
      expect(subject.normalized_content).to eq 'lol azzc lol'
      expect(subject.content).to eq @content
    end
    describe 'normalized content' do
      it 'should remove trailing white spaces' do
        @content = '  this is not cool   '
        expect(subject.normalized_content).to eq 'this is not cool'
      end

      it 'should down case all characters' do
        @content = 'WoW wWW'
        expect(subject.normalized_content).to eq 'wow www'
      end

      it 'should change polish characters to latin ones' do
        @content = 'ęążźćśńół'
        expect(subject.normalized_content).to eq 'eazzcsnol'
      end

      it 'should change all whitespaces to spaces' do
        @content = "wow\nwow\twow"
        expect(subject.normalized_content).to eq 'wow wow wow'
      end

      it 'should remove punction signs' do
        @content = 'cat, mouse? mouse! you sure.'
        expect(subject.normalized_content).to eq 'cat mouse mouse you sure'
      end

      it 'should squeeze spaces' do
        @content = 'content      content'
        expect(subject.normalized_content).to eq 'content content'
      end
    end
    describe '#to_h' do
      it 'should return all fields' do
        @content = 'content'

        expect(subject.to_h).to eq(start_time: start_time,
                                   end_time: end_time,
                                   content: 'content',
                                   normalized_content: 'content')
      end
    end
  end
end

require 'rails_helper'

module TextLines
  describe CreateMany do
    let!(:video) { create :video }
    let(:lines_invisible) { false }

    before do
      expect(SrtParser).to receive(:new).and_return(double(:parser, parse: [
        SrtParser::Line.new(0, 1, 'content1'),
        SrtParser::Line.new(4, 5, 'content2'),
        SrtParser::Line.new(10, 20, 'content3'),
        SrtParser::Line.new(30, 40, 'content4')
      ]))
    end

    subject(:interaction) { CreateMany.run(path: 'some-path', video_id: video.id, lines_invisible: lines_invisible) }

    it 'should insert many text lines' do
      expect { interaction }.to change { TextLine.count }.by(4)
    end

    describe 'created lines' do
      before { Timecop.freeze('2016-10-10 10:10') { interaction } }
      describe 'start times' do
        subject { TextLine.pluck(:start_time) }

        it { is_expected.to eq [0, 4, 10, 30] }
      end

      describe 'end times' do
        subject { TextLine.pluck(:end_time) }

        it { is_expected.to eq [1, 5, 20, 40] }
      end

      describe 'contents' do
        subject { TextLine.pluck(:content) }

        it { is_expected.to eq %w(content1 content2 content3 content4) }
      end

      describe 'normalized contents' do
        subject { TextLine.pluck(:normalized_content) }

        it { is_expected.to eq %w(content1 content2 content3 content4) }
      end

      describe 'created at' do
        subject { TextLine.pluck(:created_at) }

        it { is_expected.to eq [Time.parse('2016-10-10 10:10')] * 4 }
      end

      describe 'invisible' do
        subject { TextLine.pluck(:invisible) }

        it { is_expected.to eq [false] * 4 }

        context 'when invisible param is passed' do
          let(:lines_invisible) { true }

          it { is_expected.to eq [true] * 4 }
        end
      end
    end
  end
end

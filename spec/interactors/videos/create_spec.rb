require 'rails_helper'

module Videos
  describe Create do
    around { |example| clean_files(example) }

    subject(:interaction) do
      Create.run(
        video_path: 'spec/support/files/video-test-file.mp4',
        subtitles_path: 'some-path',
        name: 'name')
    end

    before do
      allow(TextLines::CreateMany).to receive(:run).and_return(double('lines-creation', invalid?: false, result: nil))
    end

    it 'should create Video record' do
      expect { interaction }.to change { Video.count }.by(1)
    end

    it 'should create file in videos dir' do
      expect { interaction }.to change { Dir['public/videos/*.mp4'].length }.by(1)
    end

    describe 'created video' do
      subject(:video) do
        interaction
        Video.last
      end
      its(:name) { is_expected.to eq 'name' }
    end
  end
end

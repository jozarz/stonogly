class AddVideoPartToTextLines < ActiveRecord::Migration[5.0]
  def change
    add_attachment :text_lines, :video_part_gif
  end
end

class CreateVideos < ActiveRecord::Migration[5.0]
  def change
    create_table :videos do |t|
      t.string :name
      t.timestamps
    end

    add_attachment :videos, :file
    add_belongs_to :text_lines, :video, index: true, foreign_key: { on_delete: :cascade }
  end
end

class CreateTextLines < ActiveRecord::Migration[5.0]
  def change
    create_table :text_lines do |t|
      t.integer :start_time
      t.integer :end_time

      t.text :content
      t.text :normalized_content
      t.timestamps
    end
  end
end

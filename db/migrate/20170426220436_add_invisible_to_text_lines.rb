class AddInvisibleToTextLines < ActiveRecord::Migration[5.0]
  def change
    add_column :text_lines, :invisible, :boolean, default: false, index: true
  end
end

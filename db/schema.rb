# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170426220436) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "text_lines", force: :cascade do |t|
    t.integer  "start_time"
    t.integer  "end_time"
    t.text     "content"
    t.text     "normalized_content"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.integer  "video_id"
    t.string   "video_part_gif_file_name"
    t.string   "video_part_gif_content_type"
    t.integer  "video_part_gif_file_size"
    t.datetime "video_part_gif_updated_at"
    t.boolean  "invisible",                   default: false
    t.index ["video_id"], name: "index_text_lines_on_video_id", using: :btree
  end

  create_table "videos", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
  end

  add_foreign_key "text_lines", "videos", on_delete: :cascade
end

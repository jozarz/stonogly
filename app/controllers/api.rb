class API < Grape::API
  format :json

  namespace :slack do
    mount Slack
  end
end

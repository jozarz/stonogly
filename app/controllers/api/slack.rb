class API
  class Slack < Grape::API
    params do
      requires :text, type: String
    end

    rescue_from ActiveInteraction::InvalidInteractionError do |_e|
      error!({ text: 'nie znam czegos takiego' }, 422)
    end

    get do
      line = Videos::CreatePart.run!(query: params[:text])
      url = [request.base_url, line.video_part_gif.url].join
      {
          response_type: 'in_channel',
          text: url
      }
    end

    get :inspect do
      videos = Video.all.map {|video| "<#{[request.base_url, video.file.url].join} | #{video.name}>"}
      {
          response_type: 'ephemeral',
          text: videos.join("\n")
      }
    end
  end
end

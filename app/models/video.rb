class Video < ApplicationRecord
  has_many :text_lines
  has_attached_file :file,
                    hash_secret: '2ad870c0c41ba2586f14b262095c1c49',
                    path: 'public/videos/:hash.:extension',
                    url: '/videos/:hash.:extension'

  validates_attachment :file, content_type: { content_type: 'video/mp4' }
end

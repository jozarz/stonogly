class TextLine < ApplicationRecord
  extend Normalizer

  belongs_to :video

  has_attached_file :video_part_gif,
                    hash_secret: '2ad870c0c41ba2586f14b262095c1c49',
                    path: 'public/gifs/:hash.:extension',
                    url: '/gifs/:hash.:extension'

  validates_attachment :video_part_gif, content_type: { content_type: 'image/gif' }

  def self.by_query(query)
    select_query = sanitize_sql_array([
      'text_lines.*, ts_rank_cd(to_tsvector(normalized_content), plainto_tsquery(?)) as rank',
      normalize(query)
    ])
    select(select_query)
      .where('to_tsvector(normalized_content) @@ plainto_tsquery(?)', normalize(query))
      .order('rank DESC')
  end

  def start_time_srt(last_delimiter = ',')
    time_to_srt(start_time, last_delimiter)
  end

  def end_time_srt(last_delimiter = ',')
    time_to_srt(end_time, last_delimiter)
  end

  def duration_srt(last_delimiter = ',')
    time_to_srt(duration, last_delimiter)
  end

  def to_srt(index = 1)
    "#{index}\n#{start_time_srt} --> #{end_time_srt}\n#{content}"
  end

  private

  def time_to_srt(time, last_delimiter)
    hours = time / (1.hour * 1_000)
    time = time % (1.hour * 1_000)
    minutes = time / (1.minute * 1_000)
    time = time % (1.minute * 1_000)
    seconds = time / (1.second * 1_000)
    hours, minutes, seconds = [hours, minutes, seconds].map { |t| t.to_s.rjust(2, '0') }
    milliseconds = (time % (1.second * 1_000)).to_s.rjust(3, '0')
    "#{hours}:#{minutes}:#{seconds}#{last_delimiter}#{milliseconds}"
  end

  def duration
    end_time - start_time
  end
end

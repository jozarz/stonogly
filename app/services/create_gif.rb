class CreateGif
  def initialize(text_line, video_path)
    @text_line = text_line
    @video_path = video_path
  end

  def call
    create_subtitles unless @text_line.invisible?
    system(command)
    gif_path
  end

  def command
    <<-EOS.strip.tr("\n", ' ').squeeze(' ')
      ffmpeg
        -i '#{@video_path}'
        -ss #{@text_line.start_time_srt('.')}
        -t #{@text_line.duration_srt('.')}
        -vf "#{subtitle_command} scale=300:-1"
        -loglevel #{Rails.env.test? ? 'panic' : 'info'}
        #{gif_path}
    EOS
  end

  def gif_path
    @gif_path ||= "tmp/#{SecureRandom.urlsafe_base64}.gif"
  end

  def cleanup!
    FileUtils.rm(gif_path) if File.exist?(gif_path)
  end

  private

  def subtitle_command
    "subtitles='#{@file.path}':force_style='FontName=Arial Black,Fontsize=20'," unless @text_line.invisible?
  end

  def create_subtitles
    @file = Tempfile.new('subtitles')
    @file.write(@text_line.to_srt)
    @file.close
  end
end

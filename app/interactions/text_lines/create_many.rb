module TextLines
  class CreateMany < ApplicationInteraction
    string :path
    integer :video_id
    boolean :lines_invisible, default: false

    def execute
      lines = SrtParser.new(path).parse
      lines = lines.map { |line| line_to_sql(line) }.join(",\n")
      ActiveRecord::Base.connection.execute(<<-SQL.strip
        INSERT INTO text_lines #{text_line_header} VALUES
        #{lines}
      SQL
                                           )
    rescue SrtParser::WrongFilePath
      errors.add(:path, :wrong_path)
    end

    private

    def line_to_sql(line)
      "(#{video_id}, #{line.start_time}, #{line.end_time}, '#{line.content}', '#{line.normalized_content}', '#{current_time}', '#{current_time}', #{lines_invisible})"
    end

    def current_time
      @current_time ||= Time.zone.now
    end

    def text_line_header
      '(video_id, start_time, end_time, content, normalized_content, created_at, updated_at, invisible)'
    end
  end
end

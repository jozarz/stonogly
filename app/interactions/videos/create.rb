module Videos
  class Create < ApplicationInteraction
    string :video_path
    string :subtitles_path
    string :name
    boolean :lines_invisible, default: false

    def execute
      ActiveRecord::Base.transaction do
        video = Video.create!(
          name: name,
          file: File.new(video_path)
        )
        compose(TextLines::CreateMany,
                video_id: video.id,
                path: subtitles_path,
                lines_invisible: lines_invisible
               )
      end
    rescue Errno::ENOENT
      errors.add(:video_path, :wrong_path)
    end
  end
end

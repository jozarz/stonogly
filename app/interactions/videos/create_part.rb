module Videos
  class CreatePart < ApplicationInteraction
    string :query

    validates :lines, presence: true

    def execute
      line = lines.first
      unless line.video_part_gif.present?
        creator = CreateGif.new(line, line.video.file.path)
        creator.call
        line.video_part_gif = File.new(creator.gif_path)
        line.save!
        creator.cleanup!

      end
      line
    end

    private

    def lines
      @lines ||= TextLine.by_query(query)
    end
  end
end
